﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject GameOverUI;
    public GameObject LevelCompleteUI;
    public GameObject PauseMenuUI;

    [HideInInspector]
    public bool isFollow;
    [HideInInspector]
    public int CountofShots;
    [HideInInspector]
    public int CountofEnemiesKilled;

    public static GameManager instance;
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Time.timeScale = 1f;
        isFollow = true;
        CountofEnemiesKilled = 0;
        CountofShots = 0;
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Play()
    {
        PauseMenuUI.SetActive(false);
        PlayerMovement.instance.Play();
    }


    public void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LevelComplete()
    {
        FaceBookAnalytics.instance.LogSomeEvent(SceneManager.GetActiveScene().buildIndex, CountofShots, CountofEnemiesKilled);
        isFollow = false;
        LevelCompleteUI.SetActive(true);
        Advertising.instance.ShowAd();
    }


    public void GameOver()
    {
        isFollow = false;
        GameOverUI.SetActive(true);

    }



}
