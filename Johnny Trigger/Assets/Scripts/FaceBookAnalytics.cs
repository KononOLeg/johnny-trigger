﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class FaceBookAnalytics : MonoBehaviour
{


    public static FaceBookAnalytics instance;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void LogSomeEvent(int numberLevel, int CountofShots, int CountofEnemiesKilled)
    {
        var tutParams = new Dictionary<string, object>();
        tutParams["Level"] = "Level " + numberLevel;
        tutParams["CountofShots"] = CountofShots;
        tutParams["CountofEnemiesKilled"] = CountofEnemiesKilled;

        FB.LogAppEvent(
            "Level_Info",
            parameters: tutParams
        );
    }
}


