﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float timeShoot=0f;
    public Transform firePoint;
    public Bullet bullet;

    [HideInInspector]
    public bool isFlip;

    Animator animator;
    float timer;
    bool oneShot;
    AimScript aimScript;
    GameObject player;

    void Start()
    {
        isFlip = false;
        oneShot = false;
        animator = GetComponent<Animator>();
        aimScript = GetComponent<AimScript>();
        player = GameObject.FindGameObjectWithTag("Player");
    }
    private void Update()
    {
        if (isFlip)
        {
            aimScript.Target = player.transform;
            if (timer > timeShoot && !oneShot)
            {
                Bullet b = Instantiate(bullet, firePoint.position, firePoint.rotation);
                b.enemyTag = "Player";

                oneShot = true;
            }
            timer += Time.deltaTime;
        }
    }



}
