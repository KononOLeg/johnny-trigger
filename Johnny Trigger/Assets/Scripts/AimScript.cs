﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimScript : MonoBehaviour
{

    public Transform Target;
    public Vector3 Offset;

    Animator anim;
    Transform chest;

    void Awake()
    {
        anim = GetComponent<Animator>();
        chest = anim.GetBoneTransform(HumanBodyBones.Chest);
    }

    void LateUpdate()
    {

        if (Target != null)
            chest.LookAt(Target.position);

        chest.rotation = chest.rotation * Quaternion.Euler(Offset);

    }
}