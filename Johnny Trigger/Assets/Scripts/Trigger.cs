﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{

    public string nameAnim;
    public Enemy[] enemies;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            foreach (Enemy enemy in enemies)
                enemy.isFlip = true; 

            PlayerMovement.instance.Flip(nameAnim);
        }
    }
}