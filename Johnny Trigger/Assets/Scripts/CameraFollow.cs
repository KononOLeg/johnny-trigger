﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float smoothspeed = 0.125f;
    public Vector3 offset;


    private void Update()
    {
        if(GameManager.instance.isFollow)
        {
            Vector3 diseredPosition = target.position + offset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, diseredPosition, smoothspeed);
            transform.position = smoothedPosition;
        }
      
    }
}
