﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float speed;
    [HideInInspector]
    public float direction;
    [HideInInspector]
    public string enemyTag;

    private void Update()
    {
        transform.Translate(transform.forward * Time.deltaTime * speed, Space.World);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == enemyTag)
        {
            if (other.tag == "Enemy")
            {
                GameManager.instance.CountofShots++;
                Destroy(other.gameObject);
            }
            if (other.tag == "Player")
            {
                Time.timeScale = 0f;
                GameManager.instance.GameOver();
            }
        }
    }
}

