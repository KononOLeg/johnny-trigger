﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed;
    public float gravity = -9.81f;
    public Animator anim;
    public Transform firePoint;
    public Bullet bullet;
    public AimScript aimScript;
    public LineRenderer ln;
    public LayerMask layerMask;
    [Space]
    public bool isPlay;
    public bool isFlip;

    bool isSlowMo;
    Vector3 velocity;

    public static PlayerMovement instance;
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        isSlowMo = false;
        isPlay = false;
        isFlip = false;
    }
    void Update()
    {
        if (isFlip)
        {
            RaycastHit hit;
            ln.SetPosition(0, new Vector3(firePoint.position.x,firePoint.position.y,0f));

            if (Physics.Raycast(firePoint.position, new Vector3(firePoint.forward.x, firePoint.forward.y), out hit, 20f,layerMask))
            {
                ln.SetPosition(1, hit.point);
            }
            else
            {
                Vector3 endPosition = new Vector3(firePoint.forward.x, firePoint.forward.y,0f) * 20f;
                ln.SetPosition(1, endPosition);
            }

            if (Input.GetMouseButtonDown(0))
            {
                Bullet b = Instantiate(bullet, firePoint.position,firePoint.rotation);
                b.enemyTag = "Enemy";
                GameManager.instance.CountofShots++;
            }
        }

        if (isPlay)
        {
            controller.Move(new Vector3(1f, 0f, 0f) * speed * Time.deltaTime);
            velocity.y += gravity;
            controller.Move(velocity * Time.deltaTime);
        }
    }

    public void Play()
    {
        OnisPlay();
        anim.SetBool("isPlay", true);
    }

    public void SlowMo()
    {

        if (!isSlowMo)
            Time.timeScale = 0.3f;
        else
            Time.timeScale = 1f;

        isSlowMo = !isSlowMo;
    }

    public void Flip(string nameAnim)
    {
        anim.SetTrigger(nameAnim);
    }

    public void OnisPlay()
    {
        isPlay = !isPlay;
    }
}
