﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Advertising : MonoBehaviour
{

    public static Advertising instance;

    void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        IronSource.Agent.init("b03b0ffd");
        IronSource.Agent.loadInterstitial();
        IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;
    }

    public void ShowAd()
    {
        if (IronSource.Agent.isInterstitialReady())
            IronSource.Agent.showInterstitial();
    }

    void InterstitialAdClosedEvent()
    {
        IronSource.Agent.loadInterstitial();
    }

    void OnApplicationPause(bool isPaused)
    {
        IronSource.Agent.onApplicationPause(isPaused);
    }
}
